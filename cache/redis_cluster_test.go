package storage

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestConnected(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()

	wg := sync.WaitGroup{}
	wg.Add(1)
	go ConnectToRedis(ctx, &wg, &Config{
		Host: "127.0.0.1",
		Port: 6379,
	})
	wg.Wait()

	conn := &RedisCluster{}
	err := conn.SetKey(context.Background(), "121313", "sf", 1*time.Minute)
	if err != nil {
		t.Error(err)
		return
	}

	value, err := conn.GetKey(context.Background(), "121313")
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(value)

	cancel()
	time.Sleep(1 * time.Second)

	value, err = conn.GetKey(context.Background(), "121313")
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(value)

	time.Sleep(1 * time.Hour)
}
