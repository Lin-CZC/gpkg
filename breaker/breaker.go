package breaker

import (
	"github.com/sony/gobreaker"
	"log"
	"os"
	"time"
)

type breaker struct {
	breaker *gobreaker.TwoStepCircuitBreaker
}

type option struct {
	BreakerCount    uint32        // 断路计数
	HalfOpenCount   uint32        // 半开状态最大请求数
	Interval        time.Duration // 断路器会在关闭状态下 清理计数的间隔 如果interval=0将不会清空计数器
	OpenStatePeriod time.Duration // 断路器开启时 经过 OpenStatePeriod 时间后进入到半开状态
}
type Option func(*option)

const DefaultBreaker = "default"

var StdLogger stdLogger

type stdLogger interface {
	Print(v ...interface{})
	Printf(format string, v ...interface{})
	Println(v ...interface{})
}

var breakers = make(map[string]*breaker)

func init() {
	StdLogger = log.New(os.Stdout, "[breaker] ", log.LstdFlags|log.Lshortfile) // 设置自定义日志
}

// WithBreakerCount
//
//	@Description:
//	@param breakerCount 断路计数
//	@return Option
func WithBreakerCount(breakerCount uint32) Option {
	return func(o *option) {
		o.BreakerCount = breakerCount
	}
}

func WithHalfOpenCount(halfOpenCount uint32) Option {
	return func(o *option) {
		o.HalfOpenCount = halfOpenCount
	}
}

func WithInterval(interval time.Duration) Option {
	return func(o *option) {
		o.Interval = interval
	}
}

func WithOpenStatePeriod(openStatePeriod time.Duration) Option {
	return func(o *option) {
		o.OpenStatePeriod = openStatePeriod
	}
}

// InitBreaker
//
//	@Description: 初始化并返回一个断路器实例。如果指定名称的断路器已存在，则返回该实例；否则，创建新的断路器实例
//	@param breakerName 断路器的名称
//	@param options 断路器选项
//	@return *breaker
func InitBreaker(breakerName string, options ...Option) *breaker {
	var b *breaker

	if b, ok := breakers[breakerName]; ok { // 如果存在相应的断路器 则直接返回
		return b
	} else {
		b = &breaker{}
		opt := &option{}
		halfOpenCount := uint32(2)         // 默认半开状态最大请求数 2
		breakCount := uint32(20)           // 默认断路计数 20
		interval := 5 * time.Minute        // 默认清理计数的间隔 5s
		openStatePeriod := 3 * time.Minute // 默认开启状态持续时间 3s
		for _, f := range options {
			f(opt)
		}
		if opt.BreakerCount > 0 {
			breakCount = opt.BreakerCount
		}

		if opt.HalfOpenCount > 0 {
			halfOpenCount = opt.HalfOpenCount
		}

		if opt.OpenStatePeriod > 0 {
			openStatePeriod = opt.OpenStatePeriod
		}

		if opt.Interval > 0 {
			interval = opt.Interval
		}

		cb := gobreaker.NewTwoStepCircuitBreaker(gobreaker.Settings{
			Name: breakerName,

			//MaxRequests 是半开状态下允许的最大请求数，如果MaxRequests为0，只会允许一个请求
			MaxRequests: halfOpenCount,

			//断路器会在关闭状态下，在Interval周期时间清理计数器，如果interval=0将不会清空计数器
			Interval: interval,
			//断路器开启时，经过openStatePeriod时间后进入到半开状态
			Timeout: openStatePeriod,

			//当断路器处于关闭状态时，有失败的请求进入的时候会被调用，当函数返回true时断路器会被开启。
			//这里设置了当失败请求数在 interval 时间内达到 breakCount 的个数时就会触发开启断路器
			ReadyToTrip: func(counts gobreaker.Counts) bool {
				result := counts.ConsecutiveFailures > breakCount
				if result {
					StdLogger.Printf("[%s] CircuitBreaker ConsecutiveFailures %d", breakerName, counts.ConsecutiveFailures)
				}
				return result
			},
			OnStateChange: func(name string, from, to gobreaker.State) {
				StdLogger.Printf("[%s] CircuitBreaker state change from[%s] -> to[%s]", name, from.String(), to.String())
			},
		})
		b.breaker = cb
		breakers[breakerName] = b
		StdLogger.Printf("Create CircuitBreaker name : %s ; breakCount : %d ; halfOpenCount : %d ; interval %v ;"+
			" openStatePeriod %v", breakerName, breakCount, halfOpenCount, interval, openStatePeriod)
	}
	return b
}

// GetBreaker
//
//	@Description: 获取指定名称的断路器实例。如果指定名称的断路器存在，则返回该实例；否则，抛出 panic
//	@param breakerName
//	@return *gobreaker.TwoStepCircuitBreaker
func GetBreaker(breakerName string) *gobreaker.TwoStepCircuitBreaker {
	if b, ok := breakers[breakerName]; ok {
		return b.breaker
	} else {
		panic("please call InitBreaker before !!! ")
	}
}
