package log

import (
	"context"
	"fmt"
	"math"
	"reflect"
	"runtime"
	"strconv"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"github.com/uptrace/opentelemetry-go-extra/otelutil"
)

const numAttr = 5

var (
	logSeverityKey = attribute.Key("log.severity")
	logMessageKey  = attribute.Key("log.message")
	logTemplateKey = attribute.Key("log.template")
)

var (
	std = New(NewOptions()) // 创建 zap.Logger 的轻量封装的 Logger 结构体
	mu  sync.Mutex
)

// Logger 是 zap.Logger 的轻量封装, 它结合 Ctx 添加了相关链路追踪的方法
type Logger struct {
	*zap.Logger
	skipCaller  *zap.Logger // 在调用 Logger 方法打印日志的地方再向上跳一层
	withTraceID bool        // 是否存储链路追踪的 TraceID 到 log 日志中

	minLevel         zapcore.Level // 触发打印逻辑的最小 log 等级
	errorStatusLevel zapcore.Level // 触发链路追踪修改状态的最小 log 等级

	caller      bool        // 是否在链路追踪上打印 函数名、文件名、行数信息
	stackTrace  bool        // 是否在链路追踪上打印 堆栈信息
	extraFields []zap.Field // extraFields 包含一些添加到每个 log 条目的 zap.Fields
}

//func New(logger *zap.Logger, opts ...Option) *Logger {
//	l := &Logger{
//		Logger:     logger,
//		skipCaller: logger.WithOptions(zap.AddCallerSkip(1)),
//
//		minLevel:         zap.WarnLevel,
//		errorStatusLevel: zap.ErrorLevel,
//		caller:           true,
//	}
//	for _, opt := range opts {
//		opt(l)
//	}
//	return l
//}

// WithOptions 克隆当前的 Logger, 并应用提供的 Options，然后返回结果的 Logger。可以安全地并发使用
func (l *Logger) WithOptions(opts ...zap.Option) *Logger {
	extraFields := []zap.Field{}
	// zap.New 的作用是从 .WithOptions(zap.Fields(...)) 中提取字段
	// 外部调用 zap.Fields(zap.String("baz", "baz1")) 此方法会 调用 log.core.With 方法
	zap.New(&fieldExtractorCore{extraFields: &extraFields}, opts...)
	clone := *l
	clone.Logger = l.Logger.WithOptions(opts...)
	clone.skipCaller = l.skipCaller.WithOptions(opts...)
	clone.extraFields = append(clone.extraFields, extraFields...)
	return &clone
}

// Sugar 包装了 Logger，提供了一个更符合人体工程学的、但稍微慢一些的 API
// 对 Logger 进行 Sugaring 的成本相当低廉，
// 因此在对性能敏感的代码边界上，在单个应用程序中使用 Logger 和 SugaredLogger 并在它们之间进行转换是合理的
func (l *Logger) Sugar() *SugaredLogger {
	return &SugaredLogger{
		SugaredLogger: l.Logger.Sugar(),
		skipCaller:    l.skipCaller.Sugar(),
		l:             l,
	}
}

// Clone clones the current logger applying the supplied options.
func (l *Logger) Clone(opts ...Option) *Logger {
	clone := *l
	for _, opt := range opts {
		opt(&clone)
	}
	return &clone
}

// Ctx returns a new logger with the context.
func (l *Logger) Ctx(ctx context.Context) LoggerWithCtx {
	return LoggerWithCtx{
		ctx: ctx,
		l:   l,
	}
}

func (l *Logger) DebugContext(ctx context.Context, msg string, fields ...zapcore.Field) {
	fields = l.logFields(ctx, zap.DebugLevel, msg, fields)
	l.skipCaller.Debug(msg, fields...)
}

func (l *Logger) DebugfContext(ctx context.Context, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	fields := l.logFields(ctx, zap.DebugLevel, format, []zapcore.Field{})
	l.skipCaller.Debug(msg, fields...)
}

func (l *Logger) DebugwContext(ctx context.Context, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	fields := l.logFields(ctx, zap.DebugLevel, format, []zapcore.Field{})
	l.skipCaller.Debug(msg, fields...)
}

func (l *Logger) InfoContext(ctx context.Context, msg string, fields ...zapcore.Field) {
	fields = l.logFields(ctx, zap.InfoLevel, msg, fields)
	l.skipCaller.Info(msg, fields...)
}

func (l *Logger) InfofContext(ctx context.Context, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	fields := l.logFields(ctx, zap.InfoLevel, msg, []zapcore.Field{})
	l.skipCaller.Info(msg, fields...)
}

func (l *Logger) WarnContext(ctx context.Context, msg string, fields ...zapcore.Field) {
	fields = l.logFields(ctx, zap.WarnLevel, msg, fields)
	l.skipCaller.Warn(msg, fields...)
}

func (l *Logger) WarnfContext(ctx context.Context, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	fields := l.logFields(ctx, zap.WarnLevel, msg, []zapcore.Field{})
	l.skipCaller.Info(msg, fields...)
}

func (l *Logger) ErrorContext(ctx context.Context, msg string, fields ...zapcore.Field) {
	fields = l.logFields(ctx, zap.ErrorLevel, msg, fields)
	l.skipCaller.Error(msg, fields...)
}

func (l *Logger) ErrorfContext(ctx context.Context, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	fields := l.logFields(ctx, zap.ErrorLevel, msg, []zapcore.Field{})
	l.skipCaller.Info(msg, fields...)
}

func (l *Logger) Flush() {
	_ = l.Logger.Sync()
}

func (l *Logger) DPanicContext(ctx context.Context, msg string, fields ...zapcore.Field) {
	fields = l.logFields(ctx, zap.DPanicLevel, msg, fields)
	l.skipCaller.DPanic(msg, fields...)
}

func (l *Logger) DPanicfContext(ctx context.Context, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	fields := l.logFields(ctx, zap.DPanicLevel, msg, []zapcore.Field{})
	l.skipCaller.Info(msg, fields...)
}

func (l *Logger) PanicContext(ctx context.Context, msg string, fields ...zapcore.Field) {
	fields = l.logFields(ctx, zap.PanicLevel, msg, fields)
	l.skipCaller.Panic(msg, fields...)
}

func (l *Logger) PanicfContext(ctx context.Context, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	fields := l.logFields(ctx, zap.PanicLevel, msg, []zapcore.Field{})
	l.skipCaller.Info(msg, fields...)
}

func (l *Logger) FatalContext(ctx context.Context, msg string, fields ...zapcore.Field) {
	fields = l.logFields(ctx, zap.FatalLevel, msg, fields)
	l.skipCaller.Fatal(msg, fields...)
}

func (l *Logger) FatalfContext(ctx context.Context, format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	fields := l.logFields(ctx, zap.FatalLevel, msg, []zapcore.Field{})
	l.skipCaller.Info(msg, fields...)
}

func (l *Logger) logFields(ctx context.Context, lvl zapcore.Level, msg string, fields []zapcore.Field) []zapcore.Field {
	if lvl < l.minLevel { // 判断是否小于最小的 log 触发等级
		return fields // 是 则原路返回 不做处理
	}

	switch ctx.(type) {
	case *gin.Context:
		requestID, _ := ctx.Value(KeyRequestID).(string)
		username, _ := ctx.Value(KeyUsername).(string)
		if requestID != "" {
			fields = append(fields, zap.String(KeyRequestID, requestID))
		}
		if username != "" {
			fields = append(fields, zap.String(KeyUsername, username))
		}
		ctx = ctx.(*gin.Context).Request.Context() // 获取 context.Context
	}

	span := trace.SpanFromContext(ctx) // 从当前的上下文中获取到正在处理的请求的 Span
	if !span.IsRecording() {           // 当前 Span 是否处于记录状态
		return fields
	}

	// fields: 这条日志需要添加的字段 extraFields: 任意日子都需要添加的字段
	attrs := make([]attribute.KeyValue, 0, numAttr+len(fields)+len(l.extraFields))

	for _, f := range fields {
		if f.Type == zapcore.NamespaceType { // 如果是 命名空间
			// should this be a prefix?
			continue
		}
		attrs = appendField(attrs, f) // 将 f 字段的内容 转化成为 attribute.KeyValue 添加到 attrs 中
	}

	for _, f := range l.extraFields {
		if f.Type == zapcore.NamespaceType {
			// should this be a prefix?
			continue
		}
		attrs = appendField(attrs, f)
	}

	l.log(span, lvl, msg, attrs)

	if l.withTraceID {
		traceID := span.SpanContext().TraceID().String()
		fields = append(fields, zap.String("trace_id", traceID))
	}

	return fields
}

func (l *Logger) log(span trace.Span, lvl zapcore.Level, msg string, attrs []attribute.KeyValue) {
	attrs = append(attrs, logSeverityKey.String(levelString(lvl))) // 添加日志等级
	attrs = append(attrs, logMessageKey.String(msg))               // 添加日志内容

	if l.caller {
		if fn, file, line, ok := runtimeCaller(4); ok {
			if fn != "" {
				attrs = append(attrs, semconv.CodeFunctionKey.String(fn)) // 添加 log 日志打印所在函数名
			}
			if file != "" {
				attrs = append(attrs, semconv.CodeFilepathKey.String(file)) // 添加 log 日志打印所在文件名
				attrs = append(attrs, semconv.CodeLineNumberKey.Int(line))  // 添加 log 日志打印所在行数
			}
		}
	}

	if l.stackTrace { // 是否需要保存堆栈跟踪的信息
		stackTrace := make([]byte, 2048)
		n := runtime.Stack(stackTrace, false)
		attrs = append(attrs, semconv.ExceptionStacktraceKey.String(string(stackTrace[0:n])))
	}

	span.AddEvent("log", trace.WithAttributes(attrs...)) // 在Span中添加事件

	if lvl >= l.errorStatusLevel { // 如果当前 日志等级 >= 设置的警告的日志等级
		span.SetStatus(codes.Error, msg) // 则将 span 的状态设置为 Error, 并返回其描述信息
	}
}

func runtimeCaller(skip int) (fn, file string, line int, ok bool) {
	rpc := make([]uintptr, 1)
	n := runtime.Callers(skip+1, rpc[:])
	if n < 1 {
		return
	}
	frame, _ := runtime.CallersFrames(rpc).Next()
	return frame.Function, frame.File, frame.Line, frame.PC != 0
}

//------------------------------------------------------------------------------

// LoggerWithCtx is a wrapper for Logger that also carries a context.Context.
type LoggerWithCtx struct {
	ctx context.Context
	l   *Logger
}

// Context returns logger's context.
func (l LoggerWithCtx) Context() context.Context {
	return l.ctx
}

// Logger returns the underlying logger.
func (l LoggerWithCtx) Logger() *Logger {
	return l.l
}

// ZapLogger returns the underlying zap logger.
func (l LoggerWithCtx) ZapLogger() *zap.Logger {
	return l.l.Logger
}

// Sugar returns a sugared logger with the context.
func (l LoggerWithCtx) Sugar() SugaredLoggerWithCtx {
	return SugaredLoggerWithCtx{
		ctx: l.ctx,
		s:   l.l.Sugar(),
	}
}

// WithOptions clones the current Logger, applies the supplied Options,
// and returns the resulting Logger. It's safe to use concurrently.
func (l LoggerWithCtx) WithOptions(opts ...zap.Option) LoggerWithCtx {
	return LoggerWithCtx{
		ctx: l.ctx,
		l:   l.l.WithOptions(opts...),
	}
}

// Clone 方法克隆当前的日志记录器，并应用提供的选项
func (l LoggerWithCtx) Clone() LoggerWithCtx {
	return LoggerWithCtx{
		ctx: l.ctx,
		l:   l.l.Clone(),
	}
}

// Debug logs a message at DebugLevel. The message includes any fields passed
// at the log site, as well as any fields accumulated on the logger.
func (l LoggerWithCtx) Debug(msg string, fields ...zapcore.Field) {
	fields = l.l.logFields(l.ctx, zap.DebugLevel, msg, fields)
	l.l.skipCaller.Debug(msg, fields...)
}

// Info logs a message at InfoLevel. The message includes any fields passed
// at the log site, as well as any fields accumulated on the logger.
func (l LoggerWithCtx) Info(msg string, fields ...zapcore.Field) {
	fields = l.l.logFields(l.ctx, zap.InfoLevel, msg, fields)
	l.l.skipCaller.Info(msg, fields...)
}

// Warn logs a message at WarnLevel. The message includes any fields passed
// at the log site, as well as any fields accumulated on the logger.
func (l LoggerWithCtx) Warn(msg string, fields ...zapcore.Field) {
	fields = l.l.logFields(l.ctx, zap.WarnLevel, msg, fields)
	l.l.skipCaller.Warn(msg, fields...)
}

// Error logs a message at ErrorLevel. The message includes any fields passed
// at the log site, as well as any fields accumulated on the logger.
func (l LoggerWithCtx) Error(msg string, fields ...zapcore.Field) {
	fields = l.l.logFields(l.ctx, zap.ErrorLevel, msg, fields)
	l.l.skipCaller.Error(msg, fields...)
}

// DPanic logs a message at DPanicLevel. The message includes any fields
// passed at the log site, as well as any fields accumulated on the logger.
//
// If the logger is in development mode, it then panics (DPanic means
// "development panic"). This is useful for catching errors that are
// recoverable, but shouldn't ever happen.
func (l LoggerWithCtx) DPanic(msg string, fields ...zapcore.Field) {
	fields = l.l.logFields(l.ctx, zap.DPanicLevel, msg, fields)
	l.l.skipCaller.DPanic(msg, fields...)
}

// Panic logs a message at PanicLevel. The message includes any fields passed
// at the log site, as well as any fields accumulated on the logger.
//
// The logger then panics, even if logging at PanicLevel is disabled.
func (l LoggerWithCtx) Panic(msg string, fields ...zapcore.Field) {
	fields = l.l.logFields(l.ctx, zap.PanicLevel, msg, fields)
	l.l.skipCaller.Panic(msg, fields...)
}

// Fatal logs a message at FatalLevel. The message includes any fields passed
// at the log site, as well as any fields accumulated on the logger.
//
// The logger then calls os.Exit(1), even if logging at FatalLevel is
// disabled.
func (l LoggerWithCtx) Fatal(msg string, fields ...zapcore.Field) {
	fields = l.l.logFields(l.ctx, zap.FatalLevel, msg, fields)
	l.l.skipCaller.Fatal(msg, fields...)
}

//------------------------------------------------------------------------------

// SugaredLogger 在较慢但不太冗长的 API 中封装了基本 Logger 的功能。通过使用其 Sugar 方法，可以将任何 Logger 转换为 SugaredLogger。
//
// 与 Logger 不同，SugaredLogger 不坚持于结构化日志。对于每个日志级别，它提供三种方法：一种用于松散类型的结构化日志，一种用于 println 风格的格式化，
// 以及一种用于 printf 风格的格式化。例如，SugaredLoggers 可以使用 Infow（"info with" 结构化上下文），Info 或 Infof 生成 InfoLevel 输出。
type SugaredLogger struct {
	*zap.SugaredLogger
	skipCaller *zap.SugaredLogger

	l *Logger
}

// Desugar 方法解包 SugaredLogger，暴露原始的 Logger。
// Desugaring 是比较廉价的，因此在性能敏感的代码边界，
// 单个应用程序可以同时使用 Loggers 和 SugaredLoggers，在它们之间进行转换是合理的
func (s *SugaredLogger) Desugar() *Logger {
	return s.l
}

// With adds a variadic number of fields to the logging context. It accepts a
// mix of strongly-typed Field objects and loosely-typed key-value pairs. When
// processing pairs, the first element of the pair is used as the field key
// and the second as the field value.
//
// For example,
//
//	 sugaredLogger.With(
//	   "hello", "world",
//	   "failure", errors.New("oh no"),
//	   Stack(),
//	   "count", 42,
//	   "user", User{Name: "alice"},
//	)
//
// is the equivalent of
//
//	unsugared.With(
//	  String("hello", "world"),
//	  String("failure", "oh no"),
//	  Stack(),
//	  Int("count", 42),
//	  Object("user", User{Name: "alice"}),
//	)
//
// Note that the keys in key-value pairs should be strings. In development,
// passing a non-string key panics. In production, the logger is more
// forgiving: a separate error is logged, but the key-value pair is skipped
// and execution continues. Passing an orphaned key triggers similar behavior:
// panics in development and errors in production.
func (s *SugaredLogger) With(args ...interface{}) *SugaredLogger {
	return &SugaredLogger{
		SugaredLogger: s.SugaredLogger.With(args...),
		l:             s.l,
	}
}

// Ctx returns a new sugared logger with the context.
func (s *SugaredLogger) Ctx(ctx context.Context) SugaredLoggerWithCtx {
	return SugaredLoggerWithCtx{
		ctx: ctx,
		s:   s,
	}
}

// DebugfContext uses fmt.Sprintf to log a templated message.
func (s *SugaredLogger) DebugfContext(ctx context.Context, template string, args ...interface{}) {
	s.logArgs(ctx, zap.DebugLevel, template, args)
	s.Debugf(template, args...)
}

// InfofContext uses fmt.Sprintf to log a templated message.
func (s *SugaredLogger) InfofContext(ctx context.Context, template string, args ...interface{}) {
	s.logArgs(ctx, zap.InfoLevel, template, args)
	s.Infof(template, args...)
}

// WarnfContext uses fmt.Sprintf to log a templated message.
func (s *SugaredLogger) WarnfContext(ctx context.Context, template string, args ...interface{}) {
	s.logArgs(ctx, zap.WarnLevel, template, args)
	s.Warnf(template, args...)
}

// ErrorfContext uses fmt.Sprintf to log a templated message.
func (s *SugaredLogger) ErrorfContext(ctx context.Context, template string, args ...interface{}) {
	s.logArgs(ctx, zap.ErrorLevel, template, args)
	s.Errorf(template, args...)
}

// DPanicfContext uses fmt.Sprintf to log a templated message. In development, the
// logger then panics. (See DPanicLevel for details.)
func (s *SugaredLogger) DPanicfContext(ctx context.Context, template string, args ...interface{}) {
	s.logArgs(ctx, zap.DPanicLevel, template, args)
	s.DPanicf(template, args...)
}

// PanicfContext uses fmt.Sprintf to log a templated message, then panics.
func (s *SugaredLogger) PanicfContext(ctx context.Context, template string, args ...interface{}) {
	s.logArgs(ctx, zap.PanicLevel, template, args)
	s.Panicf(template, args...)
}

// Fatalf uses fmt.Sprintf to log a templated message, then calls os.Exit.
func (s *SugaredLogger) FatalfContext(ctx context.Context, template string, args ...interface{}) {
	s.logArgs(ctx, zap.FatalLevel, template, args)
	s.Fatalf(template, args...)
}

// logArgs
//
//	@Description:
//	@receiver s
//	@param ctx
//	@param lvl log 等级
//	@param template 模板语法如: 姓名: %s\n
//	@param args 模板语法的实际参数
func (s *SugaredLogger) logArgs(ctx context.Context, lvl zapcore.Level, template string, args []interface{}) {
	if lvl < s.l.minLevel {
		return
	}
	span := trace.SpanFromContext(ctx)
	if !span.IsRecording() {
		return
	}

	attrs := make([]attribute.KeyValue, 0, numAttr+1)
	attrs = append(attrs, logTemplateKey.String(template)) // 添加 模板字段
	fmt.Printf("%s\n", "czc")
	s.l.log(span, lvl, fmt.Sprintf(template, args...), attrs)
}

// DebugwContext logs a message with some additional context. The variadic key-value
// pairs are treated as they are in With.
func (s *SugaredLogger) DebugwContext(ctx context.Context, msg string, keysAndValues ...interface{}) {
	keysAndValues = s.logKVs(ctx, zap.DebugLevel, msg, keysAndValues)
	s.Debugw(msg, keysAndValues...)
}

// InfowContext logs a message with some additional context. The variadic key-value
// pairs are treated as they are in With.
func (s *SugaredLogger) InfowContext(ctx context.Context, msg string, keysAndValues ...interface{}) {
	keysAndValues = s.logKVs(ctx, zap.InfoLevel, msg, keysAndValues)
	s.Infow(msg, keysAndValues...)
}

// WarnwContext logs a message with some additional context. The variadic key-value
// pairs are treated as they are in With.
func (s *SugaredLogger) WarnwContext(ctx context.Context, msg string, keysAndValues ...interface{}) {
	keysAndValues = s.logKVs(ctx, zap.WarnLevel, msg, keysAndValues)
	s.Warnw(msg, keysAndValues...)
}

// ErrorwContext logs a message with some additional context. The variadic key-value
// pairs are treated as they are in With.
func (s *SugaredLogger) ErrorwContext(
	ctx context.Context, msg string, keysAndValues ...interface{},
) {
	keysAndValues = s.logKVs(ctx, zap.ErrorLevel, msg, keysAndValues)
	s.Errorw(msg, keysAndValues...)
}

// DPanicwContext logs a message with some additional context. In development, the
// logger then panics. (See DPanicLevel for details.) The variadic key-value
// pairs are treated as they are in With.
func (s *SugaredLogger) DPanicwContext(
	ctx context.Context, msg string, keysAndValues ...interface{},
) {
	keysAndValues = s.logKVs(ctx, zap.DPanicLevel, msg, keysAndValues)
	s.DPanicw(msg, keysAndValues...)
}

// PanicwContext logs a message with some additional context, then panics. The
// variadic key-value pairs are treated as they are in With.
func (s *SugaredLogger) PanicwContext(ctx context.Context, msg string, keysAndValues ...interface{}) {
	keysAndValues = s.logKVs(ctx, zap.PanicLevel, msg, keysAndValues)
	s.Panicw(msg, keysAndValues...)
}

// FatalwContext logs a message with some additional context, then calls os.Exit. The
// variadic key-value pairs are treated as they are in With.
func (s *SugaredLogger) FatalwContext(ctx context.Context, msg string, keysAndValues ...interface{}) {
	keysAndValues = s.logKVs(ctx, zap.FatalLevel, msg, keysAndValues)
	s.Fatalw(msg, keysAndValues...)
}

// logKVs
//
//	@Description:
//	@receiver s
//	@param ctx
//	@param lvl log 等级
//	@param msg 打印的内容
//	@param kvs 需要打印的额外内容 key-value
//	@return []interface{}
func (s *SugaredLogger) logKVs(ctx context.Context, lvl zapcore.Level, msg string, kvs []interface{}) []interface{} {
	if lvl < s.l.minLevel {
		return kvs
	}
	span := trace.SpanFromContext(ctx)
	if !span.IsRecording() {
		return kvs
	}

	attrs := make([]attribute.KeyValue, 0, numAttr+len(kvs))

	for i := 0; i < len(kvs); i += 2 { // 遍历 kvs 步长: 2
		if key, ok := kvs[i].(string); ok {
			attrs = append(attrs, otelutil.Attribute(key, kvs[i+1])) // 存到 attrs key-value
		}
	}

	s.l.log(span, lvl, msg, attrs)

	if s.l.withTraceID {
		traceID := span.SpanContext().TraceID().String()
		kvs = append(kvs, "trace_id", traceID)
	}

	return kvs
}

//------------------------------------------------------------------------------

type SugaredLoggerWithCtx struct {
	ctx context.Context
	s   *SugaredLogger
}

// Desugar 方法解包 SugaredLogger，暴露原始的 Logger。
// Desugaring 是比较廉价的，因此在性能敏感的代码边界，
// 单个应用程序可以同时使用 Loggers 和 SugaredLoggers，在它们之间进行转换是合理的
func (s SugaredLoggerWithCtx) Desugar() *Logger {
	return s.s.Desugar()
}

// Debugf uses fmt.Sprintf to log a templated message.
func (s SugaredLoggerWithCtx) Debugf(template string, args ...interface{}) {
	s.s.logArgs(s.ctx, zap.DebugLevel, template, args)
	s.s.skipCaller.Debugf(template, args...)
}

// Infof uses fmt.Sprintf to log a templated message.
func (s SugaredLoggerWithCtx) Infof(template string, args ...interface{}) {
	s.s.logArgs(s.ctx, zap.InfoLevel, template, args)
	s.s.skipCaller.Infof(template, args...)
}

// Warnf uses fmt.Sprintf to log a templated message.
func (s SugaredLoggerWithCtx) Warnf(template string, args ...interface{}) {
	s.s.logArgs(s.ctx, zap.WarnLevel, template, args)
	s.s.skipCaller.Warnf(template, args...)
}

// Errorf uses fmt.Sprintf to log a templated message.
func (s SugaredLoggerWithCtx) Errorf(template string, args ...interface{}) {
	s.s.logArgs(s.ctx, zap.ErrorLevel, template, args)
	s.s.skipCaller.Errorf(template, args...)
}

// DPanicf uses fmt.Sprintf to log a templated message. In development, the
// logger then panics. (See DPanicLevel for details.)
func (s SugaredLoggerWithCtx) DPanicf(template string, args ...interface{}) {
	s.s.logArgs(s.ctx, zap.DPanicLevel, template, args)
	s.s.skipCaller.DPanicf(template, args...)
}

// Panicf uses fmt.Sprintf to log a templated message, then panics.
func (s SugaredLoggerWithCtx) Panicf(template string, args ...interface{}) {
	s.s.logArgs(s.ctx, zap.PanicLevel, template, args)
	s.s.skipCaller.Panicf(template, args...)
}

// Fatalf uses fmt.Sprintf to log a templated message, then calls os.Exit.
func (s SugaredLoggerWithCtx) Fatalf(template string, args ...interface{}) {
	s.s.logArgs(s.ctx, zap.FatalLevel, template, args)
	s.s.skipCaller.Fatalf(template, args...)
}

// Debugw logs a message with some additional context. The variadic key-value
// pairs are treated as they are in With.
//
// When debug-level logging is disabled, this is much faster than
//
//	s.With(keysAndValues).Debug(msg)
func (s SugaredLoggerWithCtx) Debugw(msg string, keysAndValues ...interface{}) {
	keysAndValues = s.s.logKVs(s.ctx, zap.DebugLevel, msg, keysAndValues)
	s.s.skipCaller.Debugw(msg, keysAndValues...)
}

// Infow logs a message with some additional context. The variadic key-value
// pairs are treated as they are in With.
func (s SugaredLoggerWithCtx) Infow(msg string, keysAndValues ...interface{}) {
	keysAndValues = s.s.logKVs(s.ctx, zap.InfoLevel, msg, keysAndValues)
	s.s.skipCaller.Infow(msg, keysAndValues...)
}

// Warnw logs a message with some additional context. The variadic key-value
// pairs are treated as they are in With.
func (s SugaredLoggerWithCtx) Warnw(msg string, keysAndValues ...interface{}) {
	keysAndValues = s.s.logKVs(s.ctx, zap.WarnLevel, msg, keysAndValues)
	s.s.skipCaller.Warnw(msg, keysAndValues...)
}

// Errorw logs a message with some additional context. The variadic key-value
// pairs are treated as they are in With.
func (s SugaredLoggerWithCtx) Errorw(msg string, keysAndValues ...interface{}) {
	keysAndValues = s.s.logKVs(s.ctx, zap.ErrorLevel, msg, keysAndValues)
	s.s.skipCaller.Errorw(msg, keysAndValues...)
}

// DPanicw logs a message with some additional context. In development, the
// logger then panics. (See DPanicLevel for details.) The variadic key-value
// pairs are treated as they are in With.
func (s SugaredLoggerWithCtx) DPanicw(msg string, keysAndValues ...interface{}) {
	keysAndValues = s.s.logKVs(s.ctx, zap.DPanicLevel, msg, keysAndValues)
	s.s.skipCaller.DPanicw(msg, keysAndValues...)
}

// Panicw logs a message with some additional context, then panics. The
// variadic key-value pairs are treated as they are in With.
func (s SugaredLoggerWithCtx) Panicw(msg string, keysAndValues ...interface{}) {
	keysAndValues = s.s.logKVs(s.ctx, zap.PanicLevel, msg, keysAndValues)
	s.s.skipCaller.Panicw(msg, keysAndValues...)
}

// Fatalw logs a message with some additional context, then calls os.Exit. The
// variadic key-value pairs are treated as they are in With.
func (s SugaredLoggerWithCtx) Fatalw(msg string, keysAndValues ...interface{}) {
	keysAndValues = s.s.logKVs(s.ctx, zap.FatalLevel, msg, keysAndValues)
	s.s.skipCaller.Fatalw(msg, keysAndValues...)
}

//------------------------------------------------------------------------------

// appendField 将 f 字段的内容 转化成为 attribute.KeyValue 添加到 attrs 中
func appendField(attrs []attribute.KeyValue, f zapcore.Field) []attribute.KeyValue {
	switch f.Type {
	case zapcore.BoolType:
		attr := attribute.Bool(f.Key, f.Integer == 1)
		return append(attrs, attr)

	case zapcore.Int8Type, zapcore.Int16Type, zapcore.Int32Type, zapcore.Int64Type,
		zapcore.Uint32Type, zapcore.Uint8Type, zapcore.Uint16Type, zapcore.Uint64Type,
		zapcore.UintptrType:
		attr := attribute.Int64(f.Key, f.Integer)
		return append(attrs, attr)

	case zapcore.Float32Type, zapcore.Float64Type:
		attr := attribute.Float64(f.Key, math.Float64frombits(uint64(f.Integer)))
		return append(attrs, attr)

	case zapcore.Complex64Type:
		s := strconv.FormatComplex(complex128(f.Interface.(complex64)), 'E', -1, 64)
		attr := attribute.String(f.Key, s)
		return append(attrs, attr)
	case zapcore.Complex128Type:
		s := strconv.FormatComplex(f.Interface.(complex128), 'E', -1, 128)
		attr := attribute.String(f.Key, s)
		return append(attrs, attr)

	case zapcore.StringType:
		attr := attribute.String(f.Key, f.String)
		return append(attrs, attr)
	case zapcore.BinaryType, zapcore.ByteStringType:
		attr := attribute.String(f.Key, string(f.Interface.([]byte)))
		return append(attrs, attr)
	case zapcore.StringerType:
		attr := attribute.String(f.Key, f.Interface.(fmt.Stringer).String())
		return append(attrs, attr)

	case zapcore.DurationType, zapcore.TimeType:
		attr := attribute.Int64(f.Key, f.Integer)
		return append(attrs, attr)
	case zapcore.TimeFullType:
		attr := attribute.Int64(f.Key, f.Interface.(time.Time).UnixNano())
		return append(attrs, attr)
	case zapcore.ErrorType:
		err := f.Interface.(error)
		typ := reflect.TypeOf(err).String()
		attrs = append(attrs, semconv.ExceptionTypeKey.String(typ))
		attrs = append(attrs, semconv.ExceptionMessageKey.String(err.Error()))
		return attrs
	case zapcore.ReflectType:
		attr := otelutil.Attribute(f.Key, f.Interface)
		return append(attrs, attr)
	case zapcore.SkipType:
		return attrs

	case zapcore.ArrayMarshalerType:
		var attr attribute.KeyValue
		arrayEncoder := &bufferArrayEncoder{
			stringsSlice: []string{},
		}
		err := f.Interface.(zapcore.ArrayMarshaler).MarshalLogArray(arrayEncoder)
		if err != nil {
			attr = attribute.String(f.Key+"_error", fmt.Sprintf("otelzap: unable to marshal array: %v", err))
		} else {
			attr = attribute.StringSlice(f.Key, arrayEncoder.stringsSlice)
		}
		return append(attrs, attr)

	case zapcore.ObjectMarshalerType:
		attr := attribute.String(f.Key+"_error", "otelzap: zapcore.ObjectMarshalerType is not implemented")
		return append(attrs, attr)

	default:
		attr := attribute.String(f.Key+"_error", fmt.Sprintf("otelzap: unknown field type: %v", f))
		return append(attrs, attr)
	}
}

func levelString(lvl zapcore.Level) string {
	if lvl == zapcore.DPanicLevel {
		return "PANIC"
	}
	return lvl.CapitalString()
}
