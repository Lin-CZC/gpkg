package errors

import (
	"fmt"
	"io"
	"path"
	"runtime"
	"strconv"
	"strings"
)

// Frame 表示堆栈帧内的程序计数器。由于历史原因，如果将 Frame 解释为 uintptr，其值表示程序计数器 + 1
type Frame uintptr

// pc 方法返回此帧的程序计数器 (多个帧可能具有相同的 PC 值 由于历史原因 所以需要 - 1 以便得到实际的程序计数器值)
//
//	@Description:
//	@receiver f
//	@return uintptr
func (f Frame) pc() uintptr { return uintptr(f) - 1 }

// file 方法返回包含此帧的程序计数器函数的文件的完整路径
//
//	@Description:
//	@receiver f
//	@return string
func (f Frame) file() string {
	fn := runtime.FuncForPC(f.pc()) // 返回与该程序计数器关联的函数的信息
	if fn == nil {
		return "unknown"
	}
	file, _ := fn.FileLine(f.pc())
	return file
}

// line 方法返回此帧的程序计数器函数的源代码行号
//
//	@Description:
//	@receiver f
//	@return int
func (f Frame) line() int {
	fn := runtime.FuncForPC(f.pc())
	if fn == nil {
		return 0
	}
	_, line := fn.FileLine(f.pc())
	return line
}

// name 方法返回此帧的程序计数器函数的名称
//
//	@Description:
//	@receiver f
//	@return string
func (f Frame) name() string {
	fn := runtime.FuncForPC(f.pc())
	if fn == nil {
		return "unknown"
	}
	return fn.Name()
}

// Format 方法按照 fmt.Formatter 接口的规定格式化堆栈帧
//
//	%s    打印源文件
//	%d    打印源代码行
//	%n    打印函数名
//	%v    等同于 %s:%d
//
// Format 还支持以下标志:
//
//	%+s   打印函数名和源文件的路径，相对于编译时的 GOPATH，用 \n\t 分隔（<funcname>\n\t<path>）
//	%+v   等同于 %+s:%d
func (f Frame) Format(s fmt.State, verb rune) {
	switch verb {
	case 's': // 打印源文件
		switch {
		case s.Flag('+'): // 判断 标志是否被设置成 +
			io.WriteString(s, f.name())
			io.WriteString(s, "\n\t")
			io.WriteString(s, f.file())
		default:
			io.WriteString(s, path.Base(f.file()))
		}
	case 'd': // 打印源代码行
		io.WriteString(s, strconv.Itoa(f.line()))
	case 'n': // 打印函数名
		io.WriteString(s, funcname(f.name()))
	case 'v':
		f.Format(s, 's')
		io.WriteString(s, ":")
		f.Format(s, 'd')
	}
}

// MarshalText 将堆栈追踪帧（Frame）格式化为文本字符串。输出与 fmt.Sprintf("%+v", f) 相同，但不包含换行符或制表符
func (f Frame) MarshalText() ([]byte, error) {
	name := f.name() // 函数名
	if name == "unknown" {
		return []byte(name), nil
	}
	return []byte(fmt.Sprintf("%s %s:%d", name, f.file(), f.line())), nil
}

// StackTrace 是先进先出的帧堆栈
type StackTrace []Frame

// Format 方法按照 fmt.Formatter 接口的规定格式化一组堆栈帧
//
//	%s	列出每个堆栈帧中的源文件
//	%v	列出每个堆栈帧中的源文件和行号
//
// Format 还支持以下标志:
//
//	%+v   打印每个堆栈帧的文件名、函数和行号
func (st StackTrace) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		switch {
		case s.Flag('+'):
			for _, f := range st { // 遍历 切片
				io.WriteString(s, "\n")
				f.Format(s, verb)
			}
		case s.Flag('#'): // %#v: 带有类型和 Go 语法表示的值的格式。如果是结构体或者复杂类型，会包括字段名等信息
			fmt.Fprintf(s, "%#v", []Frame(st)) // st 强制转化为 []Frame 类型的切片
		default:
			st.formatSlice(s, verb)
		}
	case 's':
		st.formatSlice(s, verb)
	}
}

// formatSlice 函数将此 StackTrace 格式化成给定缓冲区的 Frame 切片，仅在调用 '%s' 或 '%v' 时有效
func (st StackTrace) formatSlice(s fmt.State, verb rune) {
	io.WriteString(s, "[")
	for i, f := range st { // 遍历 切片
		if i > 0 {
			io.WriteString(s, " ")
		}
		f.Format(s, verb)
	}
	io.WriteString(s, "]")
}

// stack 表示程序计数器的堆栈
type stack []uintptr

func (s *stack) Format(st fmt.State, verb rune) {
	switch verb {
	case 'v':
		switch {
		case st.Flag('+'):
			for _, pc := range *s {
				f := Frame(pc)              // 强转 pc 为 Frame 类型
				fmt.Fprintf(st, "\n%+v", f) // 调用 Frame 的 Format 方法
			}
		}
	}
}

func (s *stack) StackTrace() StackTrace {
	f := make([]Frame, len(*s))
	for i := 0; i < len(f); i++ {
		f[i] = Frame((*s)[i])
	}
	return f
}

func callers() *stack {
	const depth = 32 // 堆栈的最大深度
	var pcs [depth]uintptr
	n := runtime.Callers(3, pcs[:]) // 跳过3层堆栈信息 并且填充 pcs
	var st stack = pcs[0:n]         // 实际需要填充的数据
	return &st
}

// funcname 移除函数名（由 func.Name() 报告的）中的路径前缀部分
// 如: github.com/example/pkg.SomeFunction 最终会 return SomeFunction
func funcname(name string) string {
	i := strings.LastIndex(name, "/")
	name = name[i+1:]
	i = strings.Index(name, ".")
	return name[i+1:]
}
