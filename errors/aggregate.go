package errors

import (
	"errors"
	"fmt"
)

// MessageCountMap 包含每个错误消息的出现次数
type MessageCountMap map[string]int

// Aggregate 表示包含多个错误的对象，但不一定具有单一的语义意义。
// 可以使用 errors.Is() 和聚合对象检查特定错误类型的出现。
// 不支持 Errors.As()，因为调用者可能关心与给定类型匹配的一个或多个特定错误。
type Aggregate interface {
	error
	Errors() []error
	Is(error) bool
}

// NewAggregate 将错误切片转化为 Aggregate 接口, 本身是 error 接口的实现.
// 如果切片为空, 那么返回 nil.
// 该函数会检查输入错误列表的任何元素是否为 nil, 以避免在调用 Error() 时发生空指针恐慌
func NewAggregate(errlist []error) Aggregate {
	if len(errlist) == 0 {
		return nil
	}
	// 如果 errlist 存在 空元素 则跳过
	var errs []error
	for _, e := range errlist {
		if e != nil {
			errs = append(errs, e)
		}
	}
	if len(errs) == 0 {
		return nil
	}
	return aggregate(errs)
}

// 这个类型实现了error和Errors接口。将其保持私有可以防止人们犯0个错误，这不是错误，但确实满足错误接口。
type aggregate []error

// Error is part of the error interface.
func (agg aggregate) Error() string {
	if len(agg) == 0 {
		// This should never happen, really.
		return ""
	}
	if len(agg) == 1 {
		return agg[0].Error()
	}
	seenerrs := NewString() // 获取空 map[string]Emily
	result := ""
	agg.visit(func(err error) bool {
		msg := err.Error()
		if seenerrs.Has(msg) { // 判断是否 seenerrs 有这个错误
			return false
		}
		seenerrs.Insert(msg) // 在 seenerrs 添加这个错误
		if len(seenerrs) > 1 {
			result += ", "
		}
		result += msg
		return false
	})
	if len(seenerrs) == 1 {
		return result
	}
	return "[" + result + "]"
}

func (agg aggregate) Is(target error) bool {
	return agg.visit(func(err error) bool {
		return errors.Is(err, target)
	})
}

// visit
//
//	@Description:
//	@receiver agg
//	@param f
//	@return bool
func (agg aggregate) visit(f func(err error) bool) bool {
	for _, err := range agg { // 遍历列表的错误数据
		switch err := err.(type) {
		case aggregate: // 判断是否是 深度错误数组
			if match := err.visit(f); match { // 继续递归验证
				return match
			}
		case Aggregate: // 这是实现了对应方法的类型
			for _, nestedErr := range err.Errors() {
				if match := f(nestedErr); match {
					return match
				}
			}
		default:
			if match := f(err); match {
				return match
			}
		}
	}

	return false
}

// Errors is part of the Aggregate interface.
func (agg aggregate) Errors() []error {
	return []error(agg)
}

// Matcher 用于匹配错误。如果错误匹配，则返回 true
type Matcher func(error) bool

// FilterOut  从输入错误中移除与任何匹配器匹配的所有错误。如果输入是单个错误，则只测试该错误。如果输入实现了 Aggregate 接口，错误列表将递归处理。
// 这可以用于从错误列表中移除已知的无害错误（例如 io.EOF 或 os.PathNotFound)
//
//	@Description:
//	@param err
//	@param fns
//	@return error
func FilterOut(err error, fns ...Matcher) error {
	if err == nil {
		return nil
	}
	if agg, ok := err.(Aggregate); ok { // 如果 err 实现了 Aggregate 接口
		return NewAggregate(filterErrors(agg.Errors(), fns...))
	}
	if !matchesError(err, fns...) { // 匹配不成功 fus 数组的各个函数 则返回这个错误
		return err
	}
	return nil
}

// matchesError 如果任意 Matcher return true 则 return true
func matchesError(err error, fns ...Matcher) bool {
	for _, fn := range fns {
		if fn(err) {
			return true
		}
	}
	return false
}

// filterErrors 返回所有错误（或嵌套错误，如果列表包含嵌套的 Errors），其中所有 fns 都返回 false。如果没有错误保留，将返回一个 nil 列表。
// 结果的切片将作为副作用将所有嵌套的切片展平
func filterErrors(list []error, fns ...Matcher) []error {
	result := []error{}
	for _, err := range list { // 遍历这个错误数组
		r := FilterOut(err, fns...) // 进入这个逻辑 判断 此错误能够匹配 fns 的相关函数中的一个
		if r != nil {
			result = append(result, r) // 在最终的结果填充这个错误
		}
	}
	return result
}

// Flatten 如果此 Aggregate 有着深度嵌套其他 Aggregate 让其扁平化
// 接受一个 Aggregate，该 Aggregate 可以包含任意嵌套的其他 Aggregate，并将它们全部递归展平为单个 Aggregate
//
//	@Description:
//	@param agg
//	@return Aggregate
func Flatten(agg Aggregate) Aggregate {
	result := []error{}
	if agg == nil {
		return nil
	}
	for _, err := range agg.Errors() {
		if a, ok := err.(Aggregate); ok { // 如果嵌套 其他 Aggregate
			r := Flatten(a)
			if r != nil {
				result = append(result, r.Errors()...)
			}
		} else {
			if err != nil {
				result = append(result, err)
			}
		}
	}
	return NewAggregate(result)
}

// CreateAggregateFromMessageCountMap 将 MessageCountMap 包含每个错误消息的出现次数 重新转化为 err(repeated num times) 的 字符串数组
// converts MessageCountMap Aggregate
//
//	@Description:
//	@param m
//	@return Aggregate
func CreateAggregateFromMessageCountMap(m MessageCountMap) Aggregate {
	if m == nil {
		return nil
	}
	result := make([]error, 0, len(m))
	for errStr, count := range m {
		var countStr string
		if count > 1 {
			countStr = fmt.Sprintf(" (repeated %v times)", count)
		}
		result = append(result, fmt.Errorf("%v%v", errStr, countStr))
	}
	return NewAggregate(result)
}

// Reduce 将返回 err，或者如果 err 是一个 Aggregate 且只有一个项，则返回该 Aggregate 中的第一个项
func Reduce(err error) error {
	if agg, ok := err.(Aggregate); ok && err != nil {
		switch len(agg.Errors()) {
		case 1:
			return agg.Errors()[0]
		case 0:
			return nil
		}
	}
	return err
}

// AggregateGoroutines 并行运行提供的函数，在返回的 Aggregate 中存储所有非空错误。如果所有函数都成功完成，则返回 nil
func AggregateGoroutines(funcs ...func() error) Aggregate {
	errChan := make(chan error, len(funcs))
	for _, f := range funcs {
		go func(f func() error) { errChan <- f() }(f) // 并行运行 f 函数 并返回 err 存储在 errChan
	}
	errs := make([]error, 0)
	for i := 0; i < cap(errChan); i++ { // cap 查看 channel 中的容量
		if err := <-errChan; err != nil {
			errs = append(errs, err)
		}
	}
	return NewAggregate(errs)
}

// ErrPreconditionViolated 在违反前提条件时返回
var ErrPreconditionViolated = errors.New("precondition is violated")
