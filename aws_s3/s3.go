package aws_s3

import (
	"bytes"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"io"
	"log"
	"os"
)

type Service struct {
	Client   *s3.S3 // AWS S3 客户端
	EndPoint string // S3 服务的端点地址
	Region   string // AWS 区域
}

const DefaultClientName = "default-s3-client"

var clients map[string]*Service
var StdLogger stdLogger

type stdLogger interface {
	Print(v ...interface{})
	Printf(format string, v ...interface{})
	Println(v ...interface{})
}

func init() {
	StdLogger = log.New(os.Stdout, "[breaker] ", log.LstdFlags|log.Lshortfile) // 设置自定义日志
}

// InitService
//
//	@Description: 初始化 AWS 服务，创建一个新的 Service 结构体，并将其保存在 clients 全局变量中。Service 结构体包含了一个已经配置好的 S3 客户端以及相关信息
//	@param clientID AWS 认证所需的客户端 ID
//	@param sk 密钥
//	@param token 令牌
//	@param region AWS 区域
//	@param point S3 服务的端点地址
//	@return error
func InitService(clientID, sk, token, region, point string) error {
	credential := credentials.NewStaticCredentials(clientID, sk, token)
	cfg := aws.NewConfig().WithCredentials(credential).WithRegion(region).
		WithEndpoint(point).WithS3ForcePathStyle(true).WithDisableSSL(true)
	sess, err := session.NewSession(cfg)
	if err != nil {
		return err
	}
	if len(clients) == 0 {
		clients = make(map[string]*Service, 0)
	}
	clients[clientID] = &Service{
		Client:   s3.New(sess),
		EndPoint: point,
		Region:   region,
	}
	return nil
}

// GetS3Client
//
//	@Description: 从全局的 clients 变量中获取指定名称的 Service 结构体，如果不存在则抛出 panic
//	@param clientName 标识 AWS S3 客户端的名称
//	@return *Service
func GetS3Client(clientName string) *Service {
	if client, ok := clients[clientName]; ok {
		return client
	}
	panic(fmt.Sprintf("client %s has not initial", clientName))
}

// GetObj
//
//	@Description: 使用 S3 客户端获取指定存储桶中指定键的对象，并返回其内容。如果对象不存在，则返回一个特定的错误类型 s3.ErrCodeNoSuchKey
//	@receiver s
//	@param key 是 S3 对象的键
//	@param bucket 是存储桶的名称
//	@return []byte
//	@return error
func (s *Service) GetObj(key, bucket string) ([]byte, error) {
	inputObject := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}
	out, err := s.Client.GetObject(inputObject)
	if err != nil && !IsNotFoundErr(err) {
		return nil, err
	}

	if out.Body != nil {
		res, err := io.ReadAll(out.Body)
		if err != nil {
			return nil, err
		}
		return res, nil
	}
	err = awserr.New(s3.ErrCodeNoSuchKey, "empty body", nil)
	return nil, err
}

// PutObj
//
//	@Description: 使用 S3 客户端将数据存储到指定的存储桶和键中
//	@receiver s
//	@param key 存储的对象的键
//	@param bucket 存储桶的名称
//	@param data 存储的对象的数据
//	@return error
func (s *Service) PutObj(key, bucket string, data []byte) error {
	inputObject := &s3.PutObjectInput{
		Bucket:      aws.String(bucket),
		Key:         aws.String(key),
		ContentType: aws.String("application/octet-stream"),
		Body:        bytes.NewReader(data),
	}
	out, err := s.Client.PutObject(inputObject)
	if err != nil {
		outStr := ""
		if out != nil {
			outStr = out.String()
		}
		StdLogger.Print("PutS3Object ", err, outStr)
		return err
	}
	return nil
}

// IsNotFoundErr
//
//	@Description: 判断给定的错误是否表示 S3 对象不存在的错误
//	@param err 错误对象
//	@return bool
func IsNotFoundErr(err error) bool {
	if awsErr, ok := err.(awserr.Error); ok {
		switch awsErr.Code() {
		case s3.ErrCodeNoSuchKey:
			return true
		}
	}
	return false
}
